
=====================================================================================================================================================================
How to play

Press enter on the keyboard or Start button on the controller to Spawn
Use WASD or Arrowskeys or Left Analog Stick to move
Use Spacebar or Bottom facebutton to plant bombs
Use backspace on the keyboard or Select button on the controller to Despawn

=====================================================================================================================================================================

What's working?

4 characters on single screen with their seperate names & colours (1st one has to be on the keyboard for now, will need to solve this problem later)
Bombs planting & exploding
Procedural level generation

=====================================================================================================================================================================

What's partially implemented?

Main Menu, Server Creation Menu, Lobby Menu
Steam online subsystems
LAN subsystems
Pickups

=====================================================================================================================================================================

What needs to be done?

LAN/Online play
Powerups
Scoring Systems
Optimizing, redesigning, commenting

=====================================================================================================================================================================